﻿using System;

namespace Lab5
{
    class MainClass
    {
        static void Main()
        {
            int[,] arrA = { { 4, 8, 3, 7 }, { 1, 4, 2, 5 }, { 2, 5, 3, 6 }, { 5, 3, 4, 8 } };
            int[] arrB = new int[4];

            int min = 999;
            for (int i = 0; i < 4; ++i)
            {
                min = 999;
                for (int j = 0; j < 4; ++j)
                {
                    if (min > arrA[j, i])
                        min = arrA[j, i];
                }
                arrB[i] = min;
                Console.Write(arrB[i] + " ");
            }

            Console.ReadKey();
        }
    }
}
