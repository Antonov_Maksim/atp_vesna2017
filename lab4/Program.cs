﻿using System;

namespace Lab4
{
    class MainClass
    {
        static void Main()
        {
            Console.WriteLine("'For' loop:\n");

            double forLoop, forLoopSum = 0.0;

            for (double x = 3.0; x <= 5.0; x += 0.5)
            {
                forLoop = 3 * x - 1;
                forLoopSum += (3 * x - 1);
                Console.WriteLine(forLoop);
            }

            Console.WriteLine("'For' loop sum: {0}.", forLoopSum);

            //

            Console.WriteLine("\n'While' loop:\n");

            double whileLoop, whileLoopSum = 0.0, y = 3.0;

            while (y <= 5.0)
            {
                whileLoop = Math.Exp(-y / 3) + y / (y + 1);
                whileLoopSum += (Math.Exp(-y / 3) + y / (y + 1));
                Console.WriteLine(whileLoop);
                y += 0.5;
            }

            Console.WriteLine("'While' loop sum: {0}.", whileLoopSum);
            Console.ReadKey();
        }
    }
}