﻿using System;
using System.Linq;

namespace Lab6
{
    public struct Id
    {
        public string title, author, publisher, country;
        public int publishDate, pageCount;

        public Id(string a1, string a2, string a3, int a4, string a5, int a6)
        {
            title = a1;
            author = a2;
            publisher = a3;
            publishDate = a4;
            country = a5;
            pageCount = a6;
        }
    };

    class MainClass
    {
        static void Main()
        {
            Id[] book = new Id[5];
            book[0] = new Id("Crime and Punishment", "Fyodor Dostoyevsky", "The Russian Messenger", 1866, "Russia", 983);
            book[1] = new Id("Gobseck", "Honoré de Balzac", "La Mode", 1830, "France", 95);
            book[2] = new Id("South of the Border, West of the Sun", "Haruki Murakami", "Kodansha", 1992, "Japan", 192);

            double maxPageCount = 0;
            int maxIndex = 0;

            for (int i = 0; i < 3; i++)
            {
                if (book[i].pageCount > maxPageCount)
                {
                    maxPageCount = book[i].pageCount;
                    maxIndex = i;
                }
            }

            Console.WriteLine("The book with most pages is {0}, it has {1} pages.", book[maxIndex].title, book[maxIndex].pageCount);
            Console.ReadKey();
        }
    }
}
