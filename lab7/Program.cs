﻿using System;

namespace Lab7
{
    class MainClass
    {
        public static double A(double x)
        {
            return Math.Pow(x, 2) - Math.Exp(-x);
        }

        public static double B(double x)
        {
            return Math.Log(x) + Math.Sqrt(x);
        }

        public static double C(double x)
        {
            return Math.Pow(Math.Cos(x), 2) + Math.Pow(x, 5);
        }

        public static double H(double a, double b, double c)
        {
            return Math.Pow(a, 2) + Math.Pow(b, 2) - 6 * c;
        }

        static void Main()
        {
            Console.WriteLine("Input number: ");
            double x = Convert.ToDouble(Console.ReadLine());

            double a = A(x);
            double b = B(x);
            double c = C(x);
            double h = H(a, b, c);

            Console.WriteLine("ans = {0}", h);
            Console.ReadKey();
        }
    }
}
