﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace minelem
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] arr = new int[3, 3] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

            int min = int.MaxValue;
            for(int i = 0; i < 3; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    if (arr[i, j] < min)
                        min = arr[i, j];
                    Console.Write(arr[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\nMin element of the matrix is: " + min);

            Console.ReadKey();
        }
    }
}
